    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
    }

    // Use the API call wrapper to request the member's profile data
    function getProfileData() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
    }

    // Handle the successful return from the API call
    function displayProfileData(data){
        var user = data.values[0];
        $("#name").append('Logged in as '+user.first-name+' '+user.last-name);
        document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
        document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
        document.getElementById("intro").innerHTML = user.headline;
        document.getElementById("email").innerHTML = user.emailAddress;
        document.getElementById("location").innerHTML = user.location.name;
        document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile user</a>';
        document.getElementById('profileData').style.display = 'block';

        // get company data
        var url = "/companies?format=json&is-company-admi n=true";
        IN.API.Raw(url)
        .method("GET") 
        .result(displayCompanyData)
        .error(onError);
    }
/* 
    // Use the API call wrapper to request the company's profile data
    function getCompanyData() {     
        // Masukin ID company
        cpnyID = 0; 
        IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description)?format=json")
        .method("GET")
        .result(displayCompanyData)
        .error(onError);
        saveUserData(user);

        
    } */

    function saveData(data){
        console.log(data);
    }
    function displayCompanyData(data){
        if(data.values.length == 0){
            console.log('gabisa');
            alert("The user has no companies!");
            document.getElementById('profileData').remove();
        }else{
            console.log(data);
            companyId = data.values[0].id;
            
            IN.API.Raw("/companies/" + companyId + ":(id,name,ticker,description,specialties,locations,website-url,company-type,email-domains)?format=json")
            .method("GET")
            .result(saveData)
            .error(onError)
            saveUserData(user);
        }
        
    }


    // Handle an error response from the API call
    function onError(error) {
        if(error.message == "Invalid company query request"){
            alert("The user has no company!");
            console.log("kosong");
            IN.User.logout(removeProfileData);
        }
        console.log(error);
    }

    // Destroy the session of linkedin
    function logout(){
        IN.User.logout(removeProfileData);
    }

    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
    }

    function saveUserData(userData){
        $.post('saveUserData.php', {oauth_provider:'linkedin',userData: JSON.stringify(userData)}, function(data){ return true; });
    }
    