from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class AppsOneTest(TestCase):
    def test_apps_1_url_is_exist(self):
        response = Client().get('/apps-1/')
        self.assertEqual(response.status_code, 200)
    def test_lab7_using_index_func(self):
        found = resolve('/apps-1/')
        self.assertEqual(found.func, index)