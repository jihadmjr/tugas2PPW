from django.db import models

# Create your models here.
class Company(models.Model):
	comp_id = models.CharField(max_length=400, default='not-set')
	name = models.CharField(max_length=400, default='not-set')
	email = models.CharField(max_length=100, default='not-set')
	com_type = models.CharField(max_length=100, default='not-set')
	industri = models.CharField(max_length=100, default='not-set')
	website = models.URLField()
	logo_url = models.CharField(max_length=400, default='not-set')
	size = models.CharField(max_length=100, default='not-set')
	desc = models.CharField(max_length=500, default='not-set')
	founded_year = models.CharField(max_length=100, default='not-set')
	followers = models.CharField(max_length=100, default='not-set')
	city = models.CharField(max_length=100, default='not-set')

	def __str__(self):
		return self.name