from django.conf.urls import url
from .views import index, add_company

urlpatterns = [
	url(r'^$', index, name='index'),
    url(r'^companies/(?P<comp_id>\d+)/$', index, name='index'),
    url(r'^add-company/$',add_company, name='add-company')
]