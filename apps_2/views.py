from django.shortcuts import render
from .models import Company
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt

# Create your views here.
response = {'author': 'Jeffry Aldi Permana'}

def index(request):
    response['login'] = True
    html = 'apps_2/company_profile.html'
    company = Company.object.get(id_exact=id)
    response['company'] = company
    return render(request, html, response)

@csrf_exempt
def add_company(request, id):
    comp_id = request.POST.get('id')
    if (Company.objects.filter(comp_id__exact=comp_id).exists()):
        Company.objects.filter(comp_id__exact=comp_id).delete
    name = request.POST.get('name')
    email = request.POST.get('emailDomains')
    com_type = request.POST.get('companyType.name')
    industri = request.POST.get('industries.name')
    website = request.POST.get('websiteUrl')
    logo_url = request.POST.get('logo_url')
    size = request.POST.get('employeeCountRange.name')
    desc = request.POST.get('description')
    founded_year = request.POST.get('foundedYear')
    followers = request.POST.get('numFollowers')
    city = request.POST.get('locations.values[0].adress.city')
    company = Company(comp_id=id, name=name,email=email,com_type=com_type,industri=industri,website=website,logo_url=logo_url,size=size,desc=desc,founded_year=founded_year,followers=followers,city=city)
    company.save()
    return HttpResponseRedirect('/profil/'+id+'/')