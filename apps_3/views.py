from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Forum_Lowongan
from .models import Forums


# Create your views here.
response = {'author':'Muhammad Jihad Rinaldi', 'requirement':["Web Developer", "Mobile Developer", "Software Engineer"]}
def index(request):


    forum_list = Forums.objects.all()
    forum_latest = forum_list[::-1]
    page = request.GET.get('page', 1)
    paginator = Paginator(forum_latest, 5)
    try:
        forum = paginator.page(page)
    except PageNotAnInteger:
        forum = paginator.page(1)
    except EmptyPage:
        forum = paginator.page(paginator.num_pages)
    response['forum'] = forum
    response['forum_lowongan'] = Forum_Lowongan
    html = 'apps_forums/forum.html'

    return render(request, html, response)

def add_forum(request):
    form = Forum_Lowongan(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['lowongan'] = request.POST['lowongan']
        forum = Forums(lowongan=response['lowongan'])
        forum.save()
        return HttpResponseRedirect('/apps-3/')
    else:
        return HttpResponseRedirect('/forum/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list,10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # index of current page
    index = data.number - 1
    # maximum page
    max_index = len(paginator.page_range)
    # range of 10, slicing
    start_index = index if index >= 3 else 0
    end_index = 3 if index < max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data': data, 'page_range': page_range}
    return paginate_data
