from django import forms

class CommentForm(forms.Form):
    error_messages = {
        'required': 'Tolong isi kolom comment',
    }
    description_attrs = {
		'type' : 'text',
		'placeholder' : 'submit comment here',
		'rows' : 1,
        'class': 'comment-form'
    }

    comment = forms.CharField(widget=forms.Textarea(attrs=description_attrs), required=True, label='comment')
