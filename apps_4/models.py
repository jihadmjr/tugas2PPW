from django.db import models

class Comment(models.Model):
	comment = models.TextField(max_length=160)
	name = models.TextField(max_length=40, default="")
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.comment

# Create your models here.
