from django.shortcuts import render
from .models import Comment
from .forms import CommentForm
from apps_3.models import Forums
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from apps_2.models import Company
import os
import json


#nambahin ini bar biar ga error

def index(request):
	forum = Forum.objects.all()
	companies = Company.objects.all()
	print(companies)
	#Paginator
	page = request.GET.get('page',1)
	paginate_data=paginate_page(page,forum)
	companies2 = paginate_data['data']
	page_range=paginate_data['page_range']
	response = {"companies":companies2,"page_range":page_range}
	html = "apps_4/apps_4.html"
	return render(request,html,response)

def paginate_page(page,data_list):
	paginator=Paginator(data_list,1)
	try:
		data = paginator.page(page)
	except PageNotAnInteger:
		data = paginator.page(1)
	except EmptyPage:
		data = paginator.page(paginator.num_pages)
	index = data.number-1
	max_index = len(paginator.page_range)
	start_index = index if index >=20 else 0
	end_index = 20 if index< max_index-20 else max_index

	page_range = list(paginator.page_range)[start_index:end_index]
	paginate_data = {'data':data,'page_range':page_range}
	return paginate_data
def add_comment(request, pk):
	forum = Forum.objects.get(pk=pk)
	form = CommentForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST['comment']
		comment=Comment(comment=response['comment'])
		comment.save()
		return redirect('/apps_1/') #TODO edit here
	else:
		return HttpResponseRedirect('/apps_1/') #TODO edit here

# Create your views here.
