"""bukapacil URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib import admin
import apps_1.urls as apps_1
import apps_2.urls as apps_2
import apps_3.urls as apps_3
import apps_4.urls as apps_4

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^apps-1/', include(apps_1, namespace='apps-1')),
    url(r'^apps-2/', include(apps_2, namespace='apps-2')),
    url(r'^apps-3/', include(apps_3, namespace='apps-3')),
	url(r'^apps-4/', include(apps_4, namespace='apps-4')),
	url(r'^$', RedirectView.as_view(url='/apps-1/', permanent=True)),
]
